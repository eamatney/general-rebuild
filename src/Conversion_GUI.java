import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;


	@SuppressWarnings("serial")
	public class Conversion_GUI extends JFrame  {
		
		static File[] files = null;
		File file = null;
		final JFileChooser fileSelector = new JFileChooser("Select applicable files:");
		final JFileChooser saveFileSelector = new JFileChooser("Select Save Location:");
		//final JFrame helpPage = new JFrame("Help");
		private javax.swing.JButton load;
		private javax.swing.JButton clear;
		private javax.swing.JButton help;
		private javax.swing.JButton save;
		private javax.swing.JButton close;
		private javax.swing.JButton goBack;
		protected static javax.swing.JRadioButton ABStyle;
		protected static javax.swing.JRadioButton MergedStyle;
		static javax.swing.JTextArea selectionViewer;
		private javax.swing.JTextArea helpText;
		private javax.swing.ButtonGroup styleChoice;
		private javax.swing.JScrollPane selectionScrollPane;
		private javax.swing.JScrollPane helpScrollPane;

	    public Conversion_GUI() {

	    	init_Main_GUI();
	    }

	    public final void init_Main_GUI() {

	        setLayout(null);
	        
	        setIconImage((new ImageIcon("res/images/general.jpg")).getImage());
	        
	        load = new JButton("Select Files");
	        load.setBounds(50, 45, 120, 25);
	        load.addActionListener(new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent event) {
	            	LoadActionPerformed(event);
	            }
				private void LoadActionPerformed(ActionEvent event) {
					fileSelector.setMultiSelectionEnabled(true);
					int selection = fileSelector.showOpenDialog(rootPane);
					if (selection == JFileChooser.APPROVE_OPTION) {
						files = fileSelector.getSelectedFiles();
						for(int i = 0; i < files.length; i++)
				    	{
							selectionViewer.append(files[i].toString() + '\n');	    	
				    	}}}});
	        
	        save = new JButton("Save location");
	        save.setBounds(50, 180, 120, 25);
	        save.addActionListener(new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent event) {
	            	SaveActionPerformed(event);
	            }
				private void SaveActionPerformed(ActionEvent event) {
					saveFileSelector.setDialogTitle("Specify a file to save");
					int selection = saveFileSelector.showSaveDialog(rootPane);
					if (selection == JFileChooser.APPROVE_OPTION) {
					    File location = saveFileSelector.getSelectedFile();
					    selectionViewer.append('\n' + "Save file: " + location.getAbsolutePath());
					    String userOut = location.getAbsolutePath().toString();
					    userOut = userOut + ".xls";
					    try {
							FileParser.methodOfTheMains(files, userOut);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}}});
	        
	        help = new JButton("Help");
	        help.setBounds(50, 270, 120, 25);
	        help.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					Frame helpFrame = new net.sourceforge.helpgui.gui.MainFrame(
							"/docs/help/", "slick");
					helpFrame.setVisible(true);
					helpFrame.setTitle("General Insulation Help");
					helpFrame.setSize(1100, 550);
					helpFrame.setIconImage((new ImageIcon("res/images/general.jpg")).getImage());
				}
			});
	       
	        close = new JButton("Close");
	        close.setBounds(50, 315, 120, 25);
	        close.addActionListener(new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent event) {
	            	CloseActionPerformed(event);
	            }
				private void CloseActionPerformed(ActionEvent event) {
					System.exit(0);
				}});
	        
	        clear = new JButton("Clear Files");
	        clear.setBounds(50, 225, 120, 25);
	        clear.addActionListener(new ActionListener() {
	        	@Override
	        	public void actionPerformed(ActionEvent event) {
	        		ClearActionPerformed(event);
	        	}
	        	private void ClearActionPerformed(ActionEvent event) {
	        		files = null;
	        		selectionViewer.setText(null);
	        		selectionViewer.append("Files cleared." + '\n');
			}});
	        
	        ABStyle = new JRadioButton("A Then B Style");
	       
	        ABStyle.setBounds(50, 90, 140, 25);
	            
	        MergedStyle = new JRadioButton("Merged Style");
	        MergedStyle.setBounds(50, 135, 140, 25);
	      
	              
	            
	        styleChoice = new ButtonGroup();
	        styleChoice.add(ABStyle);
	        styleChoice.add(MergedStyle);
	        
	        selectionViewer = new JTextArea();
	        selectionViewer.setEditable(false);
	        
	        selectionScrollPane = new JScrollPane(selectionViewer);
	        selectionScrollPane.setBounds(210, 45, 450, 295);
	        selectionScrollPane.setVisible(true);
	        
	        
	        add(load);
	        add(ABStyle);
	        add(MergedStyle);
	        add(save);
	        add(clear);
	        add(help);
	        add(close);
	        add(selectionScrollPane);
	        
	        

	        setTitle("Conversion Program");
	        setSize(700, 415);
	        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        setLocationRelativeTo(null);
	        setResizable(false);	
	    }
	  
	    public static void main(String[] args) {
	    	
	    	
	        
	        SwingUtilities.invokeLater(new Runnable() {
	        	
	            public void run() {
	            	
	            	try {
						UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					} catch (InstantiationException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (UnsupportedLookAndFeelException e) {
						e.printStackTrace();
					}
	            	
	                Conversion_GUI ex = new Conversion_GUI();
	                ex.setVisible(true);
	            }
	        });
	    }
	}