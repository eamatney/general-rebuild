import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;

public class FileParser {
	static String productName = "";
	static String productAmt = "";
	static String branchHeader = "";
	static String col1 = "";
	static String col2 = "";
	static String col3 = "";
	static String col4 = "";
	static String col5 = "";
	static String col6 = "";
	static String col7 = "";
	static double col8 = 0;
	static String col9 = "";
	static int counter = 1;
	static String filename = "";
	static double zeroFourTotal = 0;
	static double fiveEightTotal = 0;
	static double nineTwelveTotal = 0;
	static double thirteenTwentyFourTotal = 0;
	static ArrayList<String> zeroFour = new ArrayList<String>() ;
	static ArrayList<String> fiveEight =  new  ArrayList<String>();
	static ArrayList<String> nineTwelve = new  ArrayList<String>();
	static ArrayList<String> thirteenTwentyFour = new ArrayList<String>();
	static DecimalFormat df = new DecimalFormat("#.##");
	
	
	public static void clearArrays()
	{
		zeroFour.removeAll(zeroFour);
		fiveEight.removeAll(fiveEight);
		nineTwelve.removeAll(nineTwelve);
		thirteenTwentyFour.removeAll(thirteenTwentyFour);
		zeroFourTotal = 0;
		fiveEightTotal = 0 ;
		nineTwelveTotal = 0;
		thirteenTwentyFourTotal = 0;
	}
	
	public static void methodOfTheMains(File[] infile, String outfile) throws IOException
	{
		
		for(int i = 0; i <infile.length; i++)
		{
			
			FileReader reader = new FileReader(infile[i]);
			
			Scanner scReader = new Scanner(reader);
			String line;
			
			scReader.nextLine();
			branchHeader =scReader.nextLine();
			branchHeader = replaceMultipleSpaces(branchHeader);
			branchHeader = branchHeader.substring(0,branchHeader.length()-45).trim();
			scReader.nextLine();
			
			
					while (scReader.hasNextLine())
					{
						line = scReader.nextLine();
						lineTrimmer(line);
					}
					
					if(Conversion_GUI.ABStyle.isSelected())
					{
						massPrinter(outfile);
						clearArrays();
					}
				
			
				scReader.close();
		}
		if(Conversion_GUI.MergedStyle.isSelected())
			{
			massPrinter(outfile);
			}
	}
	
	
	public static String replaceMultipleSpaces(String s) 
	{
		
		s = s.replaceAll("  +", "\t");
		return s;
	}

	public static void additionCombinationCumulative(String x, double y)
	{
		if( x.contains("0-4"))
		{
			zeroFourTotal += y;
			zeroFour.add(x);
			Collections.sort(zeroFour);
		}
		else if( x.contains("5-8"))
		{
			fiveEightTotal += y;
			fiveEight.add(x);
			Collections.sort(fiveEight);
		}
		else if(x.contains("9-12"))
		{
			nineTwelveTotal += y;
			nineTwelve.add(x);
			Collections.sort(nineTwelve);
		}
		else
		{
			thirteenTwentyFourTotal += y;
			thirteenTwentyFour.add(x);
			Collections.sort(thirteenTwentyFour);
		}
	}
	
	
	public static void massPrinter(String filename) throws IOException
	{
		PrintWriter out = new PrintWriter(new FileWriter(filename, true));
		out.println("Bucket Totals for 0-4 Months \t" + df.format(zeroFourTotal));
		out.println("Bucket Totals for 5-8 Months \t" + df.format(fiveEightTotal));
		out.println("Bucket Totals for 9-12 Months \t" + df.format(nineTwelveTotal));
		out.println("Bucket Totals for 13-24 Months \t" +  df.format(thirteenTwentyFourTotal) + "\n");
		for (int i = 0; i < zeroFour.size()-1; i++)
		{
		out.println(zeroFour.get(i));
		}
		
		out.println("");
		out.println("Bucket Totals for 0-4 Months \t" + df.format(zeroFourTotal));
		out.println("");
		
		for (int i = 0; i < fiveEight.size()-1; i++)
		{
		out.println(fiveEight.get(i));
		}
		
		out.println("");
		out.println("Bucket Totals for 5-8 Months \t" + df.format(fiveEightTotal));
		out.println("");
		
		for (int i = 0; i < nineTwelve.size()-1; i++)
		{
		out.println(nineTwelve.get(i));
		}
		out.println("");
		out.println("Bucket Totals for 9-12 Months \t" + df.format(nineTwelveTotal));
		out.println("");

		for (int i = 0; i < thirteenTwentyFour.size()-1; i++)
		{
		out.println(thirteenTwentyFour.get(i));
		}
		
		out.println("");
		out.println("Bucket Totals for 13-24 Months \t" +  df.format(thirteenTwentyFourTotal) + "\n");
		out.println("\n\n\n");
		out.close();
	}
	
	public static void lineTrimmer(String line)
	{
		if (line.trim().startsWith("Totals")
				|| (line.trim().startsWith("Layer") || (line.trim().startsWith("Grand") || 
						(line.trim().startsWith("Write")))))
		{
			line = "";
		}
		else 
		{
			line = replaceMultipleSpaces(line);
			
			if(line.equals(""))
			{	
			}
			else
			{
			formatter(line);
			}	
		}
	}
	
	public static void formatter(String r)
	{
		String combined = "";
		
				
		if(Character.isWhitespace(r.charAt(0)))
		{
			
			String splitarray[] = r.split("\t");
			
						if (splitarray[2].contains("A"))
						{
							 col1 = productName;
							 col2 = branchHeader;
							 col3 = splitarray[1];
							 col4 = splitarray[2];
							 col5 =  "";
							 col6 = splitarray[3];
							 col7 = splitarray[4];
							 col8 = Double.parseDouble(splitarray[5].replaceAll(",", ""));
							 col9 = splitarray[6] + splitarray[7];
						}
						else
						{
						 col1 = productName;
						 col2 = branchHeader;
						 col3 = splitarray[1];
						 col4 = splitarray[2];
						 col5 = splitarray[3];
						 col6 = splitarray[4];
						 col7 = splitarray[5];
						 col8 = Double.parseDouble(splitarray[6].replaceAll(",", ""));
						 col9 = splitarray[7] + splitarray[8];
						}
						combined = col1 +"\t"+ col2 +"\t"+ col3 +"\t"+ col4 +"\t"+ col5 +"\t"+ col6 +"\t"+ col7 +"\t"+ col8 +"\t"+ col9;
						
							additionCombinationCumulative(combined, col8);
		}
		else
		{
			counter = 0;
			String splitarray[] = r.split("\t");
			if(splitarray.length >= 3)
			{
				productName = splitarray[0] + splitarray[1];
				productAmt = splitarray[2];
			}
			else
			{
			productName = splitarray[0];
			productAmt = splitarray[1];
			}
			combined = "";
		}
	}

}
